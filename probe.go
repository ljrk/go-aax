package main

import (
	"bytes"
	"fmt"
	"context"
	"os/exec"
	"encoding/json"
)


type Chapter struct {
	Id        int               `json:"id"`
	TimeBase  string            `json:"time_base"`
	Start     int               `json:"start"`
	StartTime string            `json:"start_time"`
	End       int               `json:"end"`
	EndTime   string            `json:"end_time"`
	Tags      map[string]string `json:"tags"`
}

func (a *AAXFile) ProbeChapters() ([]Chapter, error) {
	jsonData, err := a.Probe("-show_chapters")
	if err != nil {
		return nil, err
	}
	fmt.Println(string(jsonData))

	var chapters struct{ Chapters []Chapter }
	err = json.Unmarshal(jsonData, &chapters)
	if err != nil {
		return nil, err
	}

	return chapters.Chapters, nil
}

func (a *AAXFile) Probe(args ...string) ([]byte, error) {
	args = append(args, []string{"-print_format", "json", "-activation_bytes", a.ActivationBytes }...)
	args = append(args, a.Filename)
	fmt.Println(args)

	ctx := context.Background()
	cmd := exec.CommandContext(ctx, "ffprobe", args...)
	buf := bytes.NewBuffer(nil)
	cmd.Stdout = buf
	err := cmd.Run()
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
